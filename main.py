import pprint
import operator
import cognitive_face as cf
from settings import API_KEY, BASE_URL
from utils import is_rpi
from PIL import Image, ImageDraw, ImageFont
from wand.image import Image as WandImage
import wand.display


if is_rpi():
    from picamera import PiCamera


if __name__ == '__main__':

    if is_rpi():
        camera = PiCamera()
        camera.capture('result.jpg')
        camera.close()
        result_img = 'result.jpg'
    else:
        result_img = 'sample.jpg'

    cf.Key.set(API_KEY)
    BASE_URL = BASE_URL
    cf.BaseUrl.set(BASE_URL)

    img = Image.open(result_img)

    r = cf.face.detect(result_img, landmarks=False, attributes='age,gender,headPose,smile,facialHair,glasses,emotion')
    if len(r) > 0:
        print '========== Detection result =========='
        pprint.pprint(r)

        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype(font="roboto.ttf", size=24)
        draw.text(xy=(20, 20), fill=(255, 0, 0), font=font,
                  text="Gender: {}".format(r[0].get('faceAttributes', {}).get('gender', 'Unknown').capitalize()))

        draw.text(xy=(20, 60), fill=(255, 0, 0), font=font,
                  text="Age: {}".format(r[0].get('faceAttributes', {}).get('age', 'Unknown')))

        emotions = r[0].get('faceAttributes', []).get('emotion')
        em = max(emotions.items(), key=operator.itemgetter(1))[0]
        draw.text(xy=(20, 100), fill=(255, 0, 0), font=font,
                  text="Emotion: {} ({})".format(em, emotions[em]))

        # rect = r[0].get('faceRectangle')
        # if rect:
        #     draw.rectangle(xy=((rect['left'], rect['top']), (rect['width'], rect['height'])), outline='blue')

    else:
        print 'No face found!'

    img.save(result_img)

    w_img = WandImage(filename=result_img)

    wand.display.display(w_img)
